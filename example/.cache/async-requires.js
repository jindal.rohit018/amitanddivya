// prefer default export if available
const preferDefault = m => m && m.default || m

exports.components = {
  "component---gatsby-theme-wedding-src-templates-landing-js": () => import("../../gatsby-theme-wedding/src/templates/Landing.js" /* webpackChunkName: "component---gatsby-theme-wedding-src-templates-landing-js" */),
  "component---cache-dev-404-page-js": () => import("dev-404-page.js" /* webpackChunkName: "component---cache-dev-404-page-js" */),
  "component---src-pages-index-js": () => import("../src/pages/index.js" /* webpackChunkName: "component---src-pages-index-js" */)
}

