const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => m && m.default || m


exports.components = {
  "component---gatsby-theme-wedding-src-templates-landing-js": hot(preferDefault(require("/Users/rohit.jindal/Documents/Personal/amitanddivya/gatsby-theme-wedding/src/templates/Landing.js"))),
  "component---cache-dev-404-page-js": hot(preferDefault(require("/Users/rohit.jindal/Documents/Personal/amitanddivya/example/.cache/dev-404-page.js"))),
  "component---src-pages-index-js": hot(preferDefault(require("/Users/rohit.jindal/Documents/Personal/amitanddivya/example/src/pages/index.js")))
}

