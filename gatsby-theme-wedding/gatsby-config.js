module.exports = ({ contentPath = "data" }) => ({
  siteMetadata: {
    title: "Divya and Amit",
    description: "Something",
    author: "Amit Singh"
  },
  plugins: [
    {
      resolve: "gatsby-source-filesystem",
      options: {
        path: contentPath
      }
    },
    {
      resolve: "gatsby-transformer-json",
      options: {
        typeName: "Event"
      }
    }
  ]
});
