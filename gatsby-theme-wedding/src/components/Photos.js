import React from "react";
import styled from "styled-components";
import Countdown from "react-countdown-now";
import { graphql, useStaticQuery } from "gatsby";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import backgroundImg from "../images/background.jpg";
import CountItem from "./CountItem";
import One from "../images/1.jpg";
import Two from "../images/2.jpg";
import Three from "../images/3.jpg";
import Four from "../images/4.jpg";
import media from "./media";

const Container = styled.section`
  margin: 30px;
`;

const CountContainer = styled.div`
  display: flex;

  ${media.phone`
    flex-direction: column;
  `}
`;

const TitleContainer = styled.div`
  margin-bottom: 3rem;
  text-align: center;
`;

const Title = styled.h2`
  font-size: 4rem;
  margin-bottom: 0;
`;

const Subtitle = styled.sub`
  font-weight: 600;
  font-size: 1rem;
`;

const Image = styled.img`
  object-fit: cover;
  border-radius: 50%;
  width: 15rem;
  height: 15rem;
  margin: 0 auto;
  display: block;
  box-shadow: 1px 2px 1px 0 rgba(0, 0, 0, 0.1);
`;

const QUERY = graphql`
  query {
    event {
      events {
        title
      }
    }
  }
`;

function CountdownSection() {
  const {
    event: { events }
  } = useStaticQuery(QUERY);
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    autoplaySpeed: 5000,
    autoplay: true,
    slidesToScroll: 1
  };
  return (
    <Container>
      <Slider {...settings}>
        <div>
          <Image src={One} alt="image1" />
        </div>
        <div>
          <Image src={Two} alt="image2" />
        </div>
        <div>
          <Image src={Three} alt="image3" />
        </div>
        <div>
          <Image src={Four} alt="image3" />
        </div>
      </Slider>
    </Container>
  );
}

export default CountdownSection;
